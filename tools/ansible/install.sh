# base should be already installed

# Add users to sudoers on hosts
echo "sysops ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/sysops

apt update
apt upgrade -y

add-apt-repository --yes --update ppa:ansible/ansible
apt update
apt install -y ansible

# Generate ssh key - should be already set after base script
# ssh-keygen

# Insert nodes to /etc/hosts
# 192.168.1.115   node1.example.com
# 192.168.1.120   node2.example.com

# Copy keys to host machine
ssh-copy-id sysops@node1.example.com

# If you want to use custom config file (location)
cp /etc/ansible/ansible.cfg path/to/config/file

# file: config.cnf

# Next add inventory file, in other case hosts will be used
nano /path/to/catalog/inventory

[dev]
node1.example.com

[prod]
node2.example.com

# After custom config you need to set env with path to config
export ANSIBLE_CONFIG=/path/ansible.cfg
echo "export ANSIBLE_CONFIG=/path/ansible.cfg" >> ~/.profile

# Now being in created catalog with command below can check if everytthing works
ansible --version
# In config file should be path to created custom config

# If hosts are prepared than you can ping all hosts
ansible all -m ping
