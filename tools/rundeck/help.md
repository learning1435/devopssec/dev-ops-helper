#Start rundeck

`sudo service rundeckd start`

# Logs 

`tail -f /var/log/rundeck/service.log`


# Access

admin/admin

`http://localhost:4440/`

