dpkg -l | grep -i docker
apt purge -y docker-engine docker docker.io docker-ce docker-ce-cli
apt purge -y docker-ce-rootless-extras docker-scan-plugin
apt autoremove -y --purge docker-engine docker docker.io docker-ce docker-ce-rootless-extras docker-scan-plugin

rm -rf /var/lib/docker /etc/docker
rm /etc/apparmor.d/docker
groupdel docker
rm -rf /var/run/docker.sock