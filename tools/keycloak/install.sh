mkdir -rf /srv/LOCAL/keycloak

cd /srv/LOCAL

sudo wget https://downloads.jboss.org/keycloak/6.0.1/keycloak-6.0.1.tar.gz
sudo tar -xvzf keycloak-6.0.1.tar.gz
mv keycloak-6.0.1 /srv/LOCAL/keycloak

# Add group
sudo groupadd keycloak

# Add user
sudo useradd -r -g keycloak -d /opt/keycloak -s /sbin/nologin keycloak

sudo chown -R keycloak: keycloak

sudo chmod o+x /srv/LOCAL/keycloak/bin/

cd /etc/

sudo mkdir keycloak

sudo cp /srv/LOCAL/keycloak/conf/keycloak.conf /etc/keycloak/keycloak.conf

sudo chown keycloak: /srv/LOCAL/keycloak/bin/kc.sh