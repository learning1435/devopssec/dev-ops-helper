# Map ports
kubectl --namespace=traefik-playground port-forward $(kubectl get pods --namespace=traefik-playground --selector "app.kubernetes.io/name=traefik" --output=name) 9000:9000
