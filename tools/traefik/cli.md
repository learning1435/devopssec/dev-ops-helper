# Run treafik for test
`./traefik --api=true --api.dashboard=true --api.insecure=true --log --accesslog`

# Run Treafik as service
`./traefik --configFile=/etc/treafik/treafik.yml`


