$treafik = traefik_v2.7.0-rc2_linux_amd64.tar.gz

wget https://github.com/traefik/traefik/releases/download/v2.7.0-rc2/$treafik

sha256sum ./$treafik

tar -zxvf $traefik

# Give the traefik binary the ability to bind to privileged ports (e.g. 80, 443) as a non-root user:
setcap 'cap_net_bind_service=+ep' /usr/local/bin/traefik

# Set up the user, group, and directories that will be needed:
groupadd -g 321 traefik
useradd \
  -g traefik --no-user-group \
  --home-dir /var/www --no-create-home \
  --shell /usr/sbin/nologin \
  --system --uid 321 traefik

sudo mkdir /etc/traefik
sudo mkdir /etc/traefik/acme
sudo chown -R root:root /etc/traefik
sudo chown -R traefik:traefik /etc/traefik/acme

