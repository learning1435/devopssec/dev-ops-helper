# Get status for all pods
`kubectl get pods --all-namespaces`
`kubectl get pods --namespace=kube-system`

# Get events in namespace
`kubectl get events --namespace=kube-system`

# Reload pods
`kubectl -n kube-system rollout restart deploy`

# Delete pod 
`kubectl delete pod calico-kube-controllers-5ddddcdd5-2f8tw --namespace=kube-system`