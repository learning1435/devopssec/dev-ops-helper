wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

echo "deb https://artifacts.elastic.co/packages/8.x/apt stable main" > /etc/apt/sources.list.d/elastic-8.x.list

apt update

apt install elasticsearch -y

## Adjust config
nano /etc/elasticsearch/elasticsearch.yml

# Adjust JVM config
nano /etc/elasticsearch/jvm.options

# Run

systemctl daemon-reload
systemctl enable --now elasticsearch
systemctl status elasticsearch

