# Cheatsheet

### Listen all services with searched phrase
`systemctl --all | grep *worker*`

### Make action on all services matched to pattern
`systemctl --all | grep worker* | while read -r workerName; do name=$(echo "$workerName" | cut -d " " -f 1); systemctl start "$name"; done;`

