1. Create manifest file (/etc/systemd/system/<file_name>.service:

``
[Unit]
Description=The Keycloak Server
After=syslog.target network.target
Before=httpd.service
[Service]
Environment=LAUNCH_JBOSS_IN_BACKGROUND=1
EnvironmentFile=/etc/keycloak/keycloak.conf
User=keycloak
Group=keycloak
LimitNOFILE=102642
PIDFile=/var/run/keycloak/keycloak.pid
ExecStart=/opt/keycloak/bin/launch.sh $WILDFLY_MODE $WILDFLY_CONFIG $WILDFLY_BIND
StandardOutput=null
[Install]
WantedBy=multi-user.target
``

2. Reload daemon:

`sudo systemctl daemon-reload`

3. Enable service:

`sudo systemctl enable keycloak`

4. Start service:

`sudo systemctl start keycloak`

5. Check status:

`sudo systemctl status keycloak`

6. Restart failed services;

`sudo systemctl reset-failed`