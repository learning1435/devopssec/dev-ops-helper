# `curl`
In the realm of Linux, mastering the command line is essential for efficient and powerful usage of the operating system. Among the number of command line tools available, `curl` stands out as one of the most versatile and powerful utilities. Originally developed by Daniel Stenberg, `curl` is a command-line tool and library for transferring data with URLs. It supports a wide range of protocols, making it an invaluable tool for fetching, uploading, and managing data over the Internet. 

# Syntax
`curl [options] [URL]`

# Options
- --silent - Disable progress information.
- -# - Enables progress bar.
- -X - Specify HTTP method.
- -d - Specify data.
- -o - Specify output file.
- -O - Output the same as downloaded file.
- -u - Allows to add username and password for authentication. curl supports various authentication methods including Basic, Digest, and OAuth.
- -C - Allows to resume download.
- –limit-rate - Allow to limit rate. e.g: `–limit-rate 1000K`
- -T - Allows upload file.
- –libcurl - If this option is appended to any cURL command, it outputs the C source code that uses libcurl for the specified option. It is a code similar to the command line implementation.
- -a / --append - Append an already existing FTP file.

# Examples

### Fetching data
`curl https://example.com`

`curl http://site.{one, two, three}.com`

`curl ftp://ftp.example.com/file[1-20].jpeg`

### Fetch data with information about progress
`curl -# -O ftp://ftp.example.com/file.zip`

### Fetch data without progress
`curl --silent ftp://ftp.example.com/file.zip`

### Handling HTTP requests - GET data
`curl -X GET https://api.example.com/resource`

### Handling HTTP requests - POST data
`curl -X POST -d "key1=value1&key2=value2" https://api.example.com/resource`

### Specify output file
`curl -o resources.zip -X GET https://api.example.com/resources.zip`

### Specify output file
`curl -O -X GET https://api.example.com/resources.zip`

### Uploading files
`curl -T file.zip ftp://example.com/upload/`

### Handling authentication
`curl -u username:password https://example.com/api`

### Resume download
`curl -C - -O ftp://speedtest.tele2.net/1MB.zip`

### Limit rate
`curl --limit-rate 1000K -O ftp://speedtest.tele2.net/1MB.zip`

### Specify libcurl
`curl https://www.geeksforgeeks.org > log.html --libcurl code.c`

