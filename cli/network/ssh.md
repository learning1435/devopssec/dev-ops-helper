# Examples
### Add public key for authorization
`ssh -i ~/.ssh/ids_rsa root@192.168.1.100`

### Copy identification for SSH
`ssh-copy-id -i ~/.ssh/ids_rsa root@192.168.1.100`
