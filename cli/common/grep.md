# grep
Grep is a useful command to search for matching patterns in a file. grep is short for "global regular expression print". If you are a system admin who needs to scrape through log files or a developer trying to find certain occurrences in the code file, then grep is a powerful command to use.

# Syntax
`grep [OPTION...] PATTERNS [FILE...]`

# Options
- -i, --ignore-case: Ignores case distinctions in patterns and input data.
- -v, --invert-match: Selects the non-matching lines of the provided input pattern.
- -n, --line-number: Prefix each line of the matching output with the line number in the input file.
- -w: Find the exact matching word from the input file or string.
- -c: Count the number of occurrences of the provided pattern.
- -l, --files-with-matches: Prints the file name that contains the provided matching pattern.
- -L, --files-without-match: Prints the file name that does not contain the provided matching pattern.

# Examples

### Find a matching string
`grep "searched" text.txt`

### Ignore case distinctions using -i
`grep -i "Searched" text.txt`

### Select the non-matching lines using -v
`grep -v "not_searched" text.txt`

### Find the line numbers against matching input using -n
`grep -n "searched" text.txt`

### Find the exact matching word from the input file or string using -w
`grep -w "exact_search" text.txt`

### Count the occurrences' number of the provided pattern using -c
`grep -c "searched" fruits.txt`

### Scan files for matching input
`grep -l "searched" *`
