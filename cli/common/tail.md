# tail
It is the complementary of head command. The tail command, as the name implies, prints the last N number of data of the given input. By default, it prints the last 10 lines of the specified files. If more than one file name is provided then data from each file is preceded by its file name.

# Syntax
`tail [OPTION]... [FILE]...`

# Options
- -n / --lines - number of lines to display
- c / --bytes - number of bytes to display
- -q / --quiet - while using with multiple lines prevent from displaying file name
- -v / --verbose - always add file name before content
- -f / --follow - 

# Examples
### Display specified number of lines
`tail -n 5 state.txt`

### Display specified number of bytes
`tail -c 6 state.txt`

### Do not display file name (in case of many files)
`tail -q  state.txt capital.txt`

### Display file name of specified file
`tail -v state.txt`

### Display file name of specified file following new content
`tail -f state.txt`
