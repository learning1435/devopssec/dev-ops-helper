# awk
`awk` is a scripting language, and it is helpful when working in the command line. It's also a widely used command for text processing.

When using awk, you are able to select data – one or more pieces of individual text – based on a pattern you provide.

For example, some of the operations you can do with awk are searching for a specific word or pattern in a piece of text given, or even select a certain line or a certain column in a file you provide.

# Syntax
`awk '{action}' your_file_name.txt`

# Examples
### Searching for a specific word in the text
`awk '/regex pattern/{action}' your_file_name.txt`

### Print all the contents of the file using awk
`awk '{print $0}' text.txt`

### Print number of line
`awk '{print NR,$0}' information.txt `

### Print specific columns using awk #1
`awk '{print $1}' content.cvs`

### Print specific columns using awk #2
`awk '{print $1, $4}' content.cvs`

### Print specific last column using awk
`awk '{print $NF}' content.cvs`

### Print specific lines of a column
`awk '{print $1}' information.txt | head -1 `

### Print out lines with a specific pattern in awk
`awk '/^start_with/' information.txt`

### Use regular expressions in awk
`awk ' /contains_string/{print $0}' information.txt `

### Use comparison operators in awk
`awk '$3 <  40 { print $0 }' information.txt`
