# find
The find command in Linux is a dynamic utility designed for comprehensive file and directory searches within a hierarchical structure. Its adaptability allows users to search by name, size, modification time, or content, providing a flexible and potent solution. As a pivotal component of the Linux command-line toolkit, the find command caters to the nuanced needs of users, ensuring precision in file exploration and retrieval. Discover the diverse functionalities of the find command and enhance your file management efficiency on the Linux platform.

# Syntax
`find [path] [options] [expression]`

# Options
* -name pattern - Searches for files with a specific name or pattern.
* -type type - Specifies the type of file to search for (e.g., f for regular files, d for directories).
* -size [+/-]n - Searches for files based on size. `+n` finds larger files, `-n` finds smaller files. ‘n‘ measures size in characters.
* -mtime n - Finds files based on modification time. `n` represents the number of days ago.
* -exec command {} \; - Executes a command on each file found.
* -print - Displays the path names of files that match the specified criteria.
* -maxdepth levels - Restricts the search to a specified directory depth.
* -mindepth levels - Specifies the minimum directory depth for the search.
* -empty - Finds empty files and directories.
* -delete - Deletes files that match the specified criteria.
* -execdir command {} \; - Executes a command on each file found, from the directory containing the matched file.
* -iname pattern - Case-insensitive version of `-name`. Searches for files with a specific name or pattern, regardless of case.

# Examples
### Searching for a specific word in the text
`awk '/regex pattern/{action}' your_file_name.txt`

### Print all the contents of the file using awk
`awk '{print $0}' text.txt`

### Print number of line
`awk '{print NR,$0}' information.txt `

### Print specific columns using awk #1
`awk '{print $1}' content.cvs`

### Print specific columns using awk #2
`awk '{print $1, $4}' content.cvs`

### Print specific last column using awk
`awk '{print $NF}' content.cvs`

### Print specific lines of a column
`awk '{print $1}' information.txt | head -1 `

### Print out lines with a specific pattern in awk
`awk '/^start_with/' information.txt`

### Use regular expressions in awk
`awk ' /contains_string/{print $0}' information.txt `

### Use comparison operators in awk
`awk '$3 <  40 { print $0 }' information.txt`
