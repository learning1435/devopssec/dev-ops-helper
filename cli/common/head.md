# head
It is the complementary of Tail command. The head command, as the name implies, print the top N number of data of the given input. By default, it prints the first 10 lines of the specified files. If more than one file name is provided then data from each file is preceded by its file name.

# Syntax
`head [OPTION]... [FILE]...`

# Options
- -n / --lines - number of lines to display
- c / --bytes - number of bytes to display
- -q / --quiet - while using with multiple lines prevent from displaying file name
- -v / --verbose - always add file name before content

# Examples
### Display specified number of lines
`head -n 5 state.txt`

### Display specified number of bytes
`head -c 6 state.txt`

### Do not display file name (in case of many files)
`head -q  state.txt capital.txt`

### Display file name of specified file
`head -v state.txt`
