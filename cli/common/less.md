# tail
Less command is a Linux utility that can be used to read the contents of a text file one page (one screen) at a time. It has faster access because if a file is large, it doesn’t access the complete file, but accesses it page by page.

For example, if it’s a large file and you are reading it using any text editor, then the complete file will be loaded to the main memory. The less command doesn’t load the entire file but loads it part by part which makes it faster.

The less command can also be used in conjunction with other commands through pipelines. This allows us to view the output of a command directly in the less pager.

# Syntax
`less [options] filename`

# Options
* -E - Automatically exit when reaching the end of the file.
* -f - Force non-regular files to be opened.
* -F - Exit if the entire file can be displayed on the first screen.
* -g - Highlight the string that was found by the last search command.
* -G - Suppress highlighting of search matches.
* -i - Ignore cases when searching.
* -n - Suppress line numbers.
* -p pattern - Start at the first occurrence of the specified pattern in the file.
* -s - Squeeze consecutive blank lines into a single line. 

# Examples
### To find a file named “example.txt” in the home directory
`find ~ -name "example.txt"`

### Find A Specific File
`find ./GFG -name sample.txt `

### Search Files with a Pattern
`find ./GFG -name *.txt `

### Find and Confirm File Deletion
`find ./GFG -name sample.txt -exec rm -i {} \; `

### Search for Empty Files and Directories
`find ./GFG -empty`

### Search Files with Specific Permissions
`find ./GFG -perm 664`

### Display Repository Hierarchy
`find . -type d`

### Search Text Within Multiple Files
`find ./ -type f -name "*.txt" -exec grep 'Geek'  {} \;`

### Find Files by When They Were Modified
`find /path/to/search -mtime -7`

### Use Grep to Find Files Based on Content
`find . -type f -exec grep -l "pattern" {} \;`
