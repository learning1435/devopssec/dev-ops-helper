# lsof
In the world of Linux, understanding and managing open files is crucial for system administrators and users alike. The Linux operating system provides a powerful utility called lsof (List Open Files) that allows users to gain insights into the files currently open on their system. In this article, we will delve into the intricacies of the lsof command, exploring its syntax, options, and practical use cases.

Linux/Unix considers everything as a file and maintains a folder. So “Files or a File” is crucial in Linux/Unix. While working in Linux/Unix system there might be several file and folder which are being used, some of them would be visible and some not.

`lsof` command stands for List Open Files. This command provides a list of files that are opened. Basically, it gives the information to find out the files which are opened by which process. With one go it lists out all open files in output console. It cannot only list common regular files but it can list a directory, a block special file, a shared library, a character special file, a regular pipe, a named pipe, an internet socket, a UNIX domain socket, and many others. it can be combined with grep command can be used to do advanced searching and listing.

# Syntax
`lsof [option]`

# Options
- -c <process_name> - List files opened by the specified process.
- -u <username> - Display files opened by the specified user.
- -i - Show network-related information.
- -p <pid> - List files for a specific process ID.
- -t - Display only the process IDs (PIDs) rather than full details.
- -R - Display list of files opened by parent process.
- -D - Specify catalog from which files should be displayed.

# Examples

### Show file opened by a particular user
`lsof -u username`

### List all files which are opened by everyone except a specific user
`lsof -u ^root`

### List all open files by a particular process
`lsof -c mysql`

### List all open files that are opened by a particular process ID
`lsof -p 9999`

### Files opened by all other PID
`lsof -p ^9999`

### List parent process IDs
`lsof -R`

### List all opened files opened by a directory
`lsof -D /home`

### Files by network connections
`lsof -i`

### Files used by network listening on specific port
`lsof -i :80`


